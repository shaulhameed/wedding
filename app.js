var express = require('express'),
    http = require('http'),
    path = require('path'),
    mongo = require('mongoose'),
    rsvp = require('./rsvp');


var app = express();


// all environments
app.set('port', process.env.PORT || 4000);
app.use(express.bodyParser());
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.static(__dirname+ '/public'));
app.use('/stylesheet',express.static(__dirname+ '/public/stylesheet'));
app.use('/javascripts',express.static(__dirname+ '/public/javascript'));
app.use('/images',express.static(__dirname+ '/public/images'));
// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', function(req, res){
  res.sendfile(__dirname + '/public/index.html');
});
fs = require('fs');
app.post('/rsvp', function(req, res){
   
    try{
        if(req.body.email){
            rsvp.sendmail(req.body.email);
            fs.exists('database.txt', function(isExists){
                if(isExists){
                    fs.readFile('database.txt','utf-8', function (err, data) {
                        if (err) throw err;
                        var regex = new RegExp(req.body.email.trim() + ',', 'gi');
                        if(!regex.test(data)){
                            fs.appendFile('database.txt',req.body.email +"," , function (err) {
                                if (err) throw err;
                                console.log('The "data to append" was appended to file!');
                            });
                        }
                    });
                }
                else{
                    fs.appendFile('database.txt',req.body.email +"," , function (err) {
                        if (err) throw err;
                        console.log('The "data to append" was appended to file!');
                    });
                }
            });     
        }
        res.send({
            message: 'Ok'
        })
    }
    catch(e){
          res.send({
            message: 'failed'
        })
    }

})

app.listen(app.get('port'), function(){
  console.log('Application is listening to port number: ' + app.get('port'));
})