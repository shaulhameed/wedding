var mongo = require('mongoose');
var schema = mongo.Schema;

var rsvpSchema = new schema({
	email: String,
	time: String
});

var guests = undefined;
try{
	guests = mongo.model('guests');
}
catch(e){
	guests = mongo.model('guests',rsvpSchema);
}

exports.schema = guests;