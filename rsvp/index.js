/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var aws = require('aws-sdk');
var fs = require('fs');

// load aws config
aws.config.loadFromPath('config.json');

// load AWS SES
var ses = new aws.SES({apiVersion: '2010-12-01'});
var template = "";
fs.readFile(__dirname + '/template.html', 'utf-8',function(err, html){
    if(err) throw err;
    template = html;
});

module.exports.sendmail = function(emailid){
    var to = [emailid];
    var from = 'invitation@shaulwedshumaira.com';
    // this sends the email
    // @todo - add HTML version
    ses.sendEmail( { 
       Source: from, 
       Destination: { ToAddresses: to },
      
       Message: {
           Subject:{
              Data: 'Wedding Invitation - Shaul Weds Humaira'
           },
           Body: {
               Html: {
                   Data: template.trim(),
                   Charset:  "utf-8"
               }
            }
       }
    }
    , function(err, data) {
        if(err) throw err
        console.log('Email sent:');
        console.log(data);
     });
}