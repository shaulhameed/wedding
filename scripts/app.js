$(document).ready(function(e){
	var s = skrollr.init({
		edgeStrategy: 'set',
		easing: {
	        WTF: Math.random,
	        inverted: function(p) {
                return 1-p;
	        }
		}
	});

	//Registering click event.
	var emailvalidator = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;


	$("#submitemail").bind('click', function(){
		var textvalue =$('#emailid').val();
		if (emailvalidator.test(textvalue)) {
			$.post('/rsvp', {
				email: textvalue,
				time: new Date()
			}, function(response){
				$('#emailid').val('');
				$('.rsvp-content').hide();
				$('.thankyou').show();
				$('.confirmation').show();
			});
		}else{
			$('.error').show();
			setTimeout(function(){
				$('.error').hide();
			},4000)
		}
		
	})


})