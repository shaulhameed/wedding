/*global module:false*/

module.exports = function(grunt) {
    "use strict";
    
    grunt.initConfig({
        watch: {
            js:{
             files:['scripts/*.js', 'gruntfile.js'],
             tasks: ['default']
            },
            jade: {
             files: ['views/*jade'],
             tasks: ['jade']
            }
        },
        jade:{
            dev:{
                options:{
                    pretty: true
                },
                files: {
                   "public/index.html" : "views/index.jade"
                }
            }
        },
        copy:{
            dev: {
                files: [
                    {
                        src: "lib/*.js",
                        dest:"public/javascript/",
                        flatten: true,
                        expand: true
                    },
                    {
                        src: "scripts/*.js",
                        dest:"public/javascript/",
                        flatten: true,
                        expand: true
                    },
                    {
                        src: "themes/*.css",
                        dest:"public/stylesheet/",
                        flatten: true,
                        expand: true
                    },
                    {
                        src: "themes/images/*",
                        dest: "public/images/",
                        flatten: true,
                        expand: true
                    }
                ]
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-copy');
    
    grunt.registerTask('default', ['jade','copy:dev','watch']);
};